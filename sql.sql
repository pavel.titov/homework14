drop table if exists "Deposit";
drop table if exists "Subject";
drop table if exists "Currency";


create table "Currency" (
    id   serial       not null primary key,
    code char(3)      not null unique,
    name varchar(255) not null
);


create table "Subject" (
	id   serial       not null primary key,
	name varchar(255) not null
);


create table "Deposit" (
    id          serial    not null primary key,
    id_subject  int       not null,
    id_currency int       not null,
    value       int       not null,
    date        timestamp not null default(now())
);
alter table "Deposit" add constraint fk_deposit_currency foreign key (id_currency) references "Currency" (id);
alter table "Deposit" add constraint fk_deposit_subject foreign key (id_subject) references "Subject" (id);


insert into "Currency" (code, name)
values
    ('RUB', 'Russian Ruble'),
    ('USD', 'US Dollar'),
    ('AUD', 'Australian Dollar'),
    ('GBP', 'British Pound'),
    ('CNY', 'Chinese Yuan');

insert into "Subject" (name)
values
    ('Иванов Иван'),
    ('Петров Петр'),
    ('Сидоров Сидор'),
    ('Васильев Василий'),
    ('Тестов Тест');

do language 'plpgsql' $$
declare
    idRub int = (select id from "Currency" where code = 'RUB');
    idUsd int = (select id from "Currency" where code = 'USD');
begin
    insert into "Deposit" (id_subject, id_currency, value, date)
    values
        (1, idRub, 100, '2021-07-18 00:00:01'),
        (1, idRub, 200, '2021-07-18 00:00:02'),
        (1, idUsd, 200, '2021-07-19 00:00:03'),
        (5, idUsd, 50,  '2021-07-19 00:00:04'),
        (5, idUsd, 150, '2021-07-19 00:00:05');
end$$;