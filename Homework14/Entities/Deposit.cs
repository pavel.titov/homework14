﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Homework14.Entities
{
    public class Deposit
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        [ForeignKey("id_subject")]
        public Subject subject { get; set; }
        [ForeignKey("id_currency")]
        public Currency currency { get; set; }
        public int value { get; set; }
        public DateTime date { get; set; }

        public override string ToString()
        {
            return $"{id}: {subject.name} {value} {currency.code} {date}";
        }
    }
}
