﻿using Homework14.Entities;
using Microsoft.EntityFrameworkCore;

namespace Homework14
{
    public class SberbankContext : DbContext
    {
        public DbSet<Subject> Subject { get; set; }
        public DbSet<Currency> Currency { get; set; }
        public DbSet<Deposit> Deposit { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder.UseNpgsql("User ID=postgres;Password=zzz;Host=localhost;Port=5432;Database=sberbank;");
    }
}
