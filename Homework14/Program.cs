﻿using Homework14.Entities;
using System;

namespace Homework14
{
    class Program
    {
        static void Main(string[] args)
        {
            using var db = new SberbankContext();

            var c = db.Add(new Currency() { code = "JPY", name = "Japanese Yen" });
            var s = db.Add(new Subject() { name = "Алексеев Алексей" });
            var d = new Deposit()
            {
                subject = s.Entity,
                currency = c.Entity,
                value = 1000,
                date = DateTime.Now
            };
            db.Add(d);
            db.SaveChanges();


            Console.WriteLine("Субъекты:");
            foreach (var subject in db.Subject)
            {
                Console.WriteLine(subject);
            }
            Console.WriteLine();


            Console.WriteLine("Валюты:");
            foreach (var currency in db.Currency)
            {
                Console.WriteLine(currency);
            }
            Console.WriteLine();



            Console.WriteLine("Депозиты:");
            foreach (var deposit in db.Deposit)
            {
                Console.WriteLine(deposit);
            }
            Console.WriteLine();


            Console.ReadKey();
        }
    }
}
